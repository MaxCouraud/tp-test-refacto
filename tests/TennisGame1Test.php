<?php

use PHPUnit\Framework\TestCase;
include_once '../TennisGame1.php';

class TennisGame1Test extends TestCase
{
    public function testConstruct()
    {
        $player1Name = 'player1';
        $score = new TennisGame1($player1Name, 'player2');
        $this->assertEquals('player1', $player1Name);
    }
    public function testWonPoint()
    {
        //GIVEN
        $player1Name = 'player1';
        $score = new TennisGame1($player1Name, 'player2');

        //WHEN
        $score->wonPoint($player1Name);

        //THEN
        $this->assertEquals(1, $score->m_score1);
        $this->assertEquals(0, $score->m_score2);
    }

    public function testGetScore()
    {
        //GIVEN
        $player1Name = 'player1';
        $player2Name = 'player2';
        $score = new TennisGame1($player1Name, $player2Name);

        //WHEN
        $score->wonPoint($player1Name);
        $score->wonPoint( $player2Name);

        //THEN
        $this->assertEquals('Love', $score->getScore());
    }


}

